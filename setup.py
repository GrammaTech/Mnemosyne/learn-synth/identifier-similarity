import setuptools

with open("README.md", "r") as f:
    long_description = f.read()

setuptools.setup(
    name="identifier-similarity",
    author="Grammatech",
    description="API for ascertaining the similarity between identifiers.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/identifier-similarity",
    packages=["identifier_similarity.cmd", "identifier_similarity.lib"],
    package_dir={"": "src"},
    python_requires=">=3.6",
    classifiers=[
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    package_data={
        "identifier_similarity.cmd": ["schemas/*.json"],
        "identifier_similarity.lib": [
            "resources/trained_embeddings/ft_cbow.vec",
            "resources/trained_embeddings/path_based.bin",
            "resources/trained_embeddings/glove-wiki-gigaword-300/__init__.py",
            "resources/trained_embeddings/glove-wiki-gigaword-300/glove-wiki-gigaword-300.gz",
        ],
    },
    entry_points={
        "console_scripts": [
            "identifier-similarity-server = identifier_similarity.cmd.server:main",
        ],
    },
    install_requires=[
        "dpcontracts",
        "flask",
        "flask_restful",
        "gensim",
        "jsonschema",
        "numpy",
        "kitchensink @ git+https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/kitchensink@master",
    ],
    extras_require={"dev": ["black", "flake8", "pre-commit", "pytest"]},
)
