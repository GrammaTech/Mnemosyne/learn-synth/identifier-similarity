"""
Module for computing the semantic similarity between identifiers.
"""

from gensim.models import KeyedVectors
from gensim.models.keyedvectors import Word2VecKeyedVectors
from numpy.linalg import norm
from numpy import dot, ndarray
from identifier_similarity.lib import EMBEDDINGS_DIR
from importlib import import_module
from kitchensink.utility.splitter import Splitter, RoninSplitter
from logging import getLogger
from typing import Dict, List, Optional


# Upon import, load the glove-wiki-gigaword-300 model trained
# on the Wikipedia 2014 + Gigaword 5 datasets with 300-dimensional
# vectors.  The Glove embedding gives us a large vocabulary of english
# words which may be queried for similarity; Glove was shown to be a
# performant word embedding in "GloVe: Global Vectors for Word
# Representation".  Additionally, load the fast-text continuous bag of
# words (FT_CBOW) and path based (PATH_BASED) identifier embeddings upon
# import.  The identifier embeddings give us a vocabulary containing values
# likely in programs but less-so in english text, such as "substr".  However,
# the identifier embeddings have smaller vocabularies, necessitating
# supplementation with Glove.  These identifer embeddings were shown to
# be performant for finding identifier similarity in the paper "IdBench:
# Evaluating Semantic Representations of Identifier Names in Source Code"
# and excel at different tasks related to finding similarity (for instance,
# FT_CBOW excels with abbreviations).  Loading is non-trivial and should be
# performed upon initial file load instead of waiting for the first request.
_logger = getLogger(__name__)
_logger.info("Loading semantic similarity embeddings.")


GLOVE_WORD_EMBEDDING = import_module("glove-wiki-gigaword-300").load_data()
FT_CBOW_IDENTIFIER_EMBEDDING = KeyedVectors.load_word2vec_format(
    EMBEDDINGS_DIR / "ft_cbow.vec", encoding="utf-8"
)
PATH_BASED_IDENTIFIER_EMBEDDING = KeyedVectors.load_word2vec_format(
    EMBEDDINGS_DIR / "path_based.bin", binary=True, encoding="utf-8"
)

# Default value of keyword parameters
DEFAULT_EMBEDDINGS = [
    GLOVE_WORD_EMBEDDING,
    FT_CBOW_IDENTIFIER_EMBEDDING,
    PATH_BASED_IDENTIFIER_EMBEDDING,
]
DEFAULT_THRESHOLD = 0.6


def is_abbr_of(abbr: str, term: str) -> bool:
    """
    checks if @abbr is an abbreviation of @term thusly:
    1. first char should match
    2. remaining chars in @abbr should appear in order in @term

    :param abbr: non-empty string
    :param term: non-empty string
    """
    abbr = abbr.lower()
    term = term.lower()

    if not abbr or not term or abbr[0] != term[0]:
        return False

    prev_cursor = 0
    for character in abbr:
        prev_cursor = term.find(character, prev_cursor)
        if prev_cursor == -1:
            return False
        prev_cursor += 1
    return True


def embedding_similarity(
    a: str,
    b: str,
    embeddings: List = DEFAULT_EMBEDDINGS,
    threshold: float = DEFAULT_THRESHOLD,
) -> Optional[float]:
    """
    return a score in range [0.0, 1.0] representing the maximum similarity
    between two strings using embeddings, if possible

    :param a: first string
    :param b: second string
    :param embeddings: embeddings to utilize when computing similarity score
        The embeddings may be one or more of:
            GLOVE_WORD_EMBEDDING - an embedding trained on English words;
                specifically the Wikipedia 2014 + Gigaword 5 datasets
            FT_CBOW_IDENTIFIER_EMBEDDING - an fast-text continuous bag of
                words embedding trained on identifiers represented as n-grams
            PATH_BASED_IDENTIFIER_EMBEDDING - an embedding of identifiers
                using a structural, tree-based representation of code
    :param threshold: minimum similarity score between two strings in an
        embedding below which the similarity is assumed to be 0.0.
    """

    def _lookup(embedding: Word2VecKeyedVectors, a: str) -> Optional[ndarray]:
        """
        return the vector representation of @a in @embedding

        :param embedding: vector embedding of semantic simarity
        :param a: string to lookup in the embedding
        """
        # special case: for the FastText continuous bag of words
        # identifier embedding, the key is wrapped with "ID:"
        if embedding == FT_CBOW_IDENTIFIER_EMBEDDING:
            a = f'"ID:{a}"'

        return embedding[a] if a in embedding else None

    def _cos_sim(x: ndarray, y: ndarray) -> float:
        """
        return the cosine similarity between two vector representions

        :param x: vector representation of a string
        :param y: vector representation of a string
        """
        temp = x / norm(x, ord=2)
        temp2 = y / norm(y, ord=2)
        return max(0.0, round(dot(temp, temp2).item(), 2))

    def _score(embedding: Word2VecKeyedVectors, a: str, b: str) -> Optional[float]:
        """
        return a score representing the semantic similarity between @a and @b
        in the @embedding vector space

        :param embedding: vector embedding of semantic simarity
        :param a: first string
        :param b: second string
        """
        v1 = _lookup(embedding, a)
        v2 = _lookup(embedding, b)

        if v1 is not None and v2 is not None:
            return _cos_sim(v1, v2)

    # Find the similarity between a and b using the maximum score from the
    # embeddings.  If no embedding contains both strings, return None.
    scores = [_score(embedding, a, b) for embedding in embeddings]
    scores = [score for score in scores if score is not None]

    if scores:
        scores = [score for score in scores if score >= threshold] or [0.0]
        return max(scores)
    else:
        return None


def max_term_similarity(
    term: str,
    terms: List[str],
    **kwargs: Dict,
) -> float:
    """
    score a term against a list of terms generated by splitting an
    identifier into terms using name splitting algorithm

    :param term: a commonly used term
    :param terms: a identifier split into terms
    :param embeddings: embeddings to utilize when computing similarity score
    :param threshold: minimum similarity score between two strings in an
        embedding below which the similarity is assumed to be 0.0.
    :return: a score between 0.0 and 1.0 based on the maximal similarity of
        @term to @terms
    """

    def is_penalized_match() -> bool:
        """
        predicate for whether a penalty should be assessed for a single letter match.

        :return: true if a penalty should be assessed
        """
        # If the term is a single letter and the name split contains
        # multiple terms, penalize the single-letter match.
        return len(term) == 1 and (term not in terms or len(terms) > 1)

    def is_abbr_match() -> bool:
        """
        predicate for ascertaining if @term is found in @terms as an abbreviation.

        :return: true if an abbreviation is present
        """
        for term_i in terms:
            if is_abbr_of(term, term_i) or is_abbr_of(term_i, term):
                return True
        return False

    result = 0.0

    if term in terms:
        # We found an exact match.
        result = 1.0 if not is_penalized_match() else 0.25
    elif is_abbr_match():
        # We found an abbreviation.
        result = 0.95 if not is_penalized_match() else 0.25

    # After checking for exact matches or abbreviations,
    # compute the semantic similarity between term and each
    # term in the name split using vector embeddings of words
    # and identifiers.  Return highest similarity score
    # found between term and the components of the split.
    # This may improve the scores of penalized matches if
    # we can find a deeper relationship in the embeddings.
    for term_i in terms:
        similarity = embedding_similarity(term, term_i, **kwargs) or 0.0
        result = max(similarity, result)

    return result


def similarity_score(
    a: str,
    b: str,
    splitter: Splitter = RoninSplitter(),
    **kwargs: Dict,
) -> float:
    """
    compute a similarity score between two identifiers, @a and @b

    :param a: first identifier
    :param b: second identifier
    :param splitter: an instance of a name splitting algorithm to split the
        identifiers into terms
    :param embeddings: embeddings to utilize when computing similarity score
    :param threshold: minimum similarity score between two strings in an
        embedding below which the similarity is assumed to be 0.0.
    :return: score between 0.0 and 1.0
    """

    # Split the two identifiers into terms (e.g. "JSON_Encoder" ->
    # ["JSON", "Encoder"]).  Order the identifiers such that b_terms
    # always contains an equal or greater number of terms than
    # a_terms to ensure the score computation is symmetric.
    terms = [splitter.split_lower(a), splitter.split_lower(b)]
    terms = sorted(terms)  # first sort lexigraphically for tiebreakers
    terms = sorted(terms, key=len)  # stable sort by list length
    a_terms, b_terms = terms

    # Compute the similarity score by finding the average
    # similarity between each term in @a with the terms in @b.
    score = 0.0
    for term in a_terms:
        score += max_term_similarity(term, b_terms, **kwargs)
    return score / len(b_terms)
