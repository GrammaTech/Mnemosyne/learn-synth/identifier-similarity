import pathlib
import sys

EMBEDDINGS_DIR = pathlib.Path(__file__).parent / "resources" / "trained_embeddings"
sys.path.append(str(EMBEDDINGS_DIR))
