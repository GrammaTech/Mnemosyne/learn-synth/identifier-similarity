"""
REST server for computing the similarity between two identifiers.
"""

import argparse
import logging

from pathlib import Path
from json import dumps, loads
from typing import Dict

from dpcontracts import ensure, require
from flask import Flask, Response, request
from flask_restful import Api, Resource
from jsonschema import Draft7Validator

from kitchensink.utility.filesystem import slurp
from kitchensink.utility.splitter import (  # noqa: F401
    HeuristicSplitter,
    PythonHeuristicSplitter,
)

logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)
from identifier_similarity.lib.similarity import (  # noqa: E402, F401
    similarity_score,
    GLOVE_WORD_EMBEDDING,
    FT_CBOW_IDENTIFIER_EMBEDDING,
    PATH_BASED_IDENTIFIER_EMBEDDING,
)

DEFAULT_PORT = 3042
DEFAULT_STDIO_FLAG = False
SCHEMAS_DIR = Path(__file__).parent / "schemas"


class SimilarityScore(Resource):
    """
    rest resource for computing the similarity between two identifiers
    """

    POST_INPUT_SCHEMA = Draft7Validator(
        loads(slurp(SCHEMAS_DIR / "similarity-score-input-schema.json"))
    )
    POST_OUTPUT_SCHEMA = Draft7Validator(
        loads(slurp(SCHEMAS_DIR / "similarity-score-output-schema.json"))
    )

    @staticmethod
    @require(
        "input must satisfy the JSON schema",
        lambda args: SimilarityScore.POST_INPUT_SCHEMA.is_valid(args.body),
    )
    @ensure(
        "output must satisfy the JSON schema",
        lambda args, results: SimilarityScore.POST_OUTPUT_SCHEMA.is_valid(results),
    )
    def compute(body: Dict) -> Dict:
        def preprocess(body: Dict) -> Dict:
            if "embeddings" in body:
                body["embeddings"] = [globals()[e] for e in body["embeddings"]]
            if "splitter" in body:
                body["splitter"] = globals()[body["splitter"]]
            return body

        return {"score": similarity_score(**preprocess(body))}

    def post(self) -> Response:
        body = request.get_json(force=True)

        return Response(
            dumps(SimilarityScore.compute(body)),
            status=200,
            mimetype="application/json",
        )


def main():
    parser = argparse.ArgumentParser(__doc__)

    parser.add_argument(
        "--port",
        type=int,
        default=DEFAULT_PORT,
        help="Port to listen to REST requests on.",
    )
    parser.add_argument(
        "--stdio",
        action="store_true",
        default=DEFAULT_STDIO_FLAG,
        help="Run LSP server in STDIO mode.",
    )

    args = parser.parse_args()
    logging.info("Starting identifier similarity server.")

    if args.stdio:
        while True:
            try:
                body = loads(input())
                result = SimilarityScore.compute(body)
                print(dumps(result))
            except EOFError:
                break
            except Exception as e:
                logging.error(e)
    else:
        app = Flask(__name__)
        api = Api(app)
        api.add_resource(SimilarityScore, "/similarity-score")

        app.run("0.0.0.0", port=args.port)


if __name__ == "__main__":
    main()
