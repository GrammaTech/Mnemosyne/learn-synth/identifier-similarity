# Description

API for ascertaining similarity between identifiers.

# Requirements

The identifier-similarity repository requires python 3.6 or higher.
You will also need git-lfs which may be installed using the
instructions at https://git-lfs.github.com/.

# Installation

## Developer Install

If you are working on the development of this repository or
are a client looking for more frequent updates, you may wish
to install the package using the instructions given below.

The dependencies for this repository may be installed by executing
`pip3 install -r requirements.txt`.  If you are contributing you
will want to install the pre-commit hooks found in the
[pre-commit config](./pre-commit-config.yaml) by executing
`pre-commit install`.

After installing dependencies, you must place the repository's
src/ directory on your $PYTHONPATH.  From the base of this
repository, this may be accomplished with the following command:

```
export PYTHONPATH=$PWD/src:$PYTHONPATH
```

Finally, you will need to clone the
[kitchensink](https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/kitchensink)
repository, install its requirements, and place its src/ directory on
your $PYTHONPATH.  The kitchensink repository is a dependency of this
repository under current development.  This setup may be done with the
following sequence of commands, to be run from the parent directory
of this repository:

```
git clone git@gitlab.com:GrammaTech/Mnemosyne/learn-synth/kitchensink.git
cd kitchensink
pip3 install -r requirements.txt
export PYTHONPATH=$PWD/src:$PYTHONPATH
```

## Client Install

If you are using the repository solely as a client, you may
create a python wheel file and install it by executing the
following sequence of commands:

```
python3 setup.py bdist_wheel --universal
pip3 install dist/*
```

Once installed, an `identifier-similarity-server` command will be
added to your $PATH which may be invoked directly using the same
interface and options described in the usage section below.

# Command Line Interface

## Identifier Similarity Server

For developers, the `server.py` script in `src/identifier_similariy/cmd`
defines a command line interface for this tool.  Help documentation is
available by running `server.py --help`.

The REST server may listen for requests on a given port (`--port`) or
on standard input (`--stdio`).  In both modes, the server takes requests
with a json body similar to the following: `{"a": "foo", "b": "bar"}`.
An example is given below:

```
curl -X POST \
        http://localhost:3042/similarity-score \
        -H 'Content-Type: application/json' \
        -d '{"a": "foo", "b": "bar"}'
```

The returned JSON has the form `{"score": float[0.0, 1.0]}`

Beyond this, the client may specify the splitter to utilize when breaking
the identifiers into terms (e.g. `"JSON_Encoding_Version"` ->
`["JSON", "Encoding", "Version"]`), the word or identifier embeddings to
utilize in score calculation, and the threshold similarity score from an
embedding below which we assume two terms are unrelated.  A full
description of input parameters and the acceptable range of values for
them may be found in the `similarity-score-input-schema.json` schema.

### LISP API

A Common LISP wrapper around the similarity server may be found at
https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/identifier-similarity/.

### FAQ

#### I am getting the error 'dpcontracts.PreconditionError: input must satisfy the JSON schema'

Ensure your input JSON matches the schema defined in
`similarity-score-input-schema.json`.

#### I am getting the error '<msg>: line [0-9]+ column [0-9]+ (char [0-9]+)`

Ensure your input JSON is well formed.

#### I am getting the error '{"message": "Internal Server Error"}'

Ensure your input JSON is well formed and matches the schema defined in
`similarity-score-input-schema.json`.
