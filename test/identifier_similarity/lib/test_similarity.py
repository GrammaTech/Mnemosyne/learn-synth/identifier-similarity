"""
Unit tests for the semantic similarity module.
"""

from math import isclose
from identifier_similarity.lib.similarity import (
    is_abbr_of,
    embedding_similarity,
    max_term_similarity,
    similarity_score,
    GLOVE_WORD_EMBEDDING,
    FT_CBOW_IDENTIFIER_EMBEDDING,
    PATH_BASED_IDENTIFIER_EMBEDDING,
)


def test_is_abbr_of():
    """
    Tests to ensure `is_abbbr_of` properly identifies abbreviations.
    """
    assert is_abbr_of("m", "m")
    assert is_abbr_of("m", "msg")
    assert is_abbr_of("m", "MSG")
    assert is_abbr_of("msg", "message")
    assert is_abbr_of("msg", "MESSAGE")
    assert not is_abbr_of("n", "MESSAGE")


def test_embedding_similarity():
    """
    Tests to ensure `embedding_similarity` returns a reasonable measure
    of similarity between two terms if those terms appear in the vocabulary
    for the embeddings.
    """
    assert isclose(embedding_similarity("substr", "substring"), 0.97, rel_tol=0.01)
    assert isclose(embedding_similarity("start", "begin"), 0.85, rel_tol=0.01)
    assert isclose(embedding_similarity("foo", "bar"), 0.80, rel_tol=0.01)
    assert isclose(embedding_similarity("ridx", "idx"), 0.74, rel_tol=0.01)
    assert isclose(embedding_similarity("golf", "dinosaurs"), 0.0, rel_tol=0.01)


def test_embedding_similarity_out_of_vocab():
    """
    Test to ensure `embedding_similarity` returns None if the terms do not
    appear in the vocabulary of the embeddings.
    """
    assert embedding_similarity("out-of-vocab1", "out-of-vocab2") is None
    assert embedding_similarity("out-of-vocab1", "start") is None
    assert embedding_similarity("start", "out-of-vocab1") is None


def test_embedding_similarity_threshold_parameter():
    """
    Tests to ensure `embedding_similarity` respects the threshold parameter and
    floors results less than the threshold to 0.0.
    """
    assert isclose(
        embedding_similarity("federal", "tax", threshold=0.4), 0.48, rel_tol=0.01
    )
    assert isclose(
        embedding_similarity("federal", "tax", threshold=0.5), 0.00, rel_tol=0.01
    )


def test_embedding_similarity_embeddings_parameter():
    """
    Tests to ensure `embedding_similarity` respects the embeddings parameter and
    only returns results based on the given embedding(s).
    """
    embeddings = [GLOVE_WORD_EMBEDDING]
    assert embedding_similarity("substr", "substring", embeddings=embeddings) is None

    embeddings = [FT_CBOW_IDENTIFIER_EMBEDDING]
    assert isclose(
        embedding_similarity("substr", "substring", embeddings=embeddings),
        0.91,
        rel_tol=0.01,
    )

    embeddings = [PATH_BASED_IDENTIFIER_EMBEDDING]
    assert isclose(
        embedding_similarity("substr", "substring", embeddings=embeddings),
        0.97,
        rel_tol=0.01,
    )

    embeddings = [
        GLOVE_WORD_EMBEDDING,
        FT_CBOW_IDENTIFIER_EMBEDDING,
        PATH_BASED_IDENTIFIER_EMBEDDING,
    ]
    assert isclose(
        embedding_similarity("substr", "substring", embeddings=embeddings),
        0.97,
        rel_tol=0.01,
    )


def test_max_term_similarity():
    """
    Tests to ensure the `max_term_similarity` function correctly scores
    the maximum similarity between a term and list of terms split
    from an identifier name.
    """
    # The term foo is totally dissimilar to dinosaurs so score 0.
    sim = max_term_similarity("foo", ["dinosaurs"])
    assert isclose(sim, 0.0)

    # The term foo is semantically similar to bar so score the similarity.
    sim = max_term_similarity("foo", ["bar"])
    assert isclose(sim, 0.8, rel_tol=0.01)

    # The term msg is an abbreviation of message so score 0.95.
    sim = max_term_similarity("msg", ["message"])
    assert isclose(sim, 0.95)

    # The term and name are the same so score 1.0.
    sim = max_term_similarity("foo", ["foo"])
    assert isclose(sim, 1.0)

    # The term foo appears in foo_bar so score 1.0.
    sim = max_term_similarity("foo", ["foo", "bar"])
    assert isclose(sim, 1.0)


def test_similarity_score():
    """
    Tests to ensure the `similarity_score` function correctly
    scores the similarity between two identifiers.
    """
    # Score 1.0 in the case of an exact batch.
    sim = similarity_score("f", "f")
    assert isclose(sim, 1.0)

    sim = similarity_score("foo", "foo")
    assert isclose(sim, 1.0)

    sim = similarity_score("foo_bar", "foo_bar")
    assert isclose(sim, 1.0)

    # Score 0.95 in the case of an abbreviation.
    sim = similarity_score("msg", "message")
    assert isclose(sim, 0.95)

    # Score based on semantic similarity when the terms either match or
    # are semantically similar.
    sim = similarity_score("baz_bar", "foo_bar")
    assert isclose(sim, 0.89, rel_tol=0.01)

    sim = similarity_score("start", "begin")
    assert isclose(sim, 0.85, rel_tol=0.01)

    # Score 0.125 ((0.25 + 0.0)/2 or 75% penalty) when half the terms match
    # and the match is only a single letter.
    sim = similarity_score("f_bar", "f_dinosaur")
    assert isclose(sim, 0.125)

    # Score 0.0 when there is no match or semantic similarity.
    sim = similarity_score("bar", "dinosaur")
    assert isclose(sim, 0.0)
